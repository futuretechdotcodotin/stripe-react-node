const app = require("express")();
const stripe = require("stripe")('sk_test_p5dc9s7iZ3mYBJoPhhST3Cr600Bfsgs6T6');
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

    // Pass to next layer of middleware
    next();
});
app.post("/", async function (req, res) {
    console.log("aa gya")
    const token = req.body.token; // Using Express
    if(token){
        try {
            const charge = await stripe.charges.create({
                amount: 999,
                currency: 'eur',
                description: 'Example charge',
                source: token,
            });

            res.send(charge);
        }catch(err){
            res.send(err);
        }
    }else{
        res.status(400).send();
    }
    

});

// app listening on port 4000
app.listen(4000, () => {
    console.log('server is running on port 4000');
});