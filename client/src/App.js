import React from 'react';
import logo from './logo.svg';
import './App.css';

import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';

import CheckoutForm from './component/CheckoutForm';

const stripePromise = loadStripe("pk_test_6QMJXg8KSmneR2rUAS7Ewowg00EyQZiDxD");

function App() {
  return (
    <Elements stripe={stripePromise}>
      <CheckoutForm />
    </Elements>
  );
}

export default App;
